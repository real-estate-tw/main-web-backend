sudo vi /etc/mysql/mysql.conf.d/mysqld.cnf

...
[mysqld]
character-set-server=utf8
collation-server=utf8_general_ci
...
sudo /etc/init.d/mysql restart
mysql -u root -p
mysql> show variables like "%character%";


mysql -u root -p
CREATE DATABASE web;
show variables like '%char%';

create database web;
use web;
create table RealTest ( country VARCHAR(20) NOT NULL, value BIGINT NOT NULL);
insert into RealTest (country, value)
values ("Taipei",3768221565029);
insert into RealTest (country, value)
values ("NewTaipei", 3992101377372);
insert into RealTest (country, value)
values ("Taichung", 2553817915416);
insert into RealTest (country, value)
values ("Taoyuan", 1957913501257);
insert into RealTest (country, value)
values ("Kaohsiung", 1707366686826);
insert into RealTest (country, value)
values ("Tainan", 901344910660);

SELECT * FROM RealTest;

drop database web
