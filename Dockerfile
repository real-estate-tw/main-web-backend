FROM ubuntu:20.04

ENV env=test
RUN apt-get update && apt-get -y install  vim tree wget 
RUN apt-get update && apt-get -y install  python3-pip
WORKDIR  /usr
COPY ./ /opt/flask
RUN tree /opt/flask
#RUN pip3 install -r /opt/flask/requirements.txt
RUN pip3 install flask pandas pymongo PyMySQL
EXPOSE 5000
CMD ["python3", "/opt/flask/app.py"]

# RUN export CLOUD_SDK_REPO="cloud-sdk-$(lsb_release -c -s)"&& \ 
#  echo "deb http://packages.cloud.google.com/apt $CLOUD_SDK_REPO main" |  tee -a /etc/apt/sources.list.d/google-cloud-sdk.list \
#  &&  curl https://packages.cloud.google.com/apt/doc/apt-key.gpg |  apt-key add -  \
# && apt update &&  apt  install -y  google-cloud-sdk 
