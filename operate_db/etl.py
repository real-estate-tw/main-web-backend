import csv 
import os
allfiles=[]
down1file=[]
startPathh="/home/u/city"

#tree /home/u/cityscsv
# (base) u@ubuntu:~/cityscsv$ tree /home/u/cityscsv
# /home/u/cityscsv
# ├── 102
# │   ├── s1
# │   │   ├── 102S1
# │   │   ├── A_lvr_land_A.csv
# │   │   ├── B_lvr_land_A.csv
# │   │   ├── C_lvr_land_A.csv
# │   │   ├── D_lvr_land_A.csv
# │   │   ├── E_lvr_land_A.csv
# │   │   ├── F_lvr_land_A.csv
# │   │   ├── G_lvr_land_A.csv
# │   │   ├── H_lvr_land_A.csv
# │   │   ├── I_lvr_land_A.csv
# │   │   ├── J_lvr_land_A.csv
# │   │   ├── K_lvr_land_A.csv
# │   │   ├── manifest.csv


#this def could wolk all fold & file under (rootPath/startPathh) and return all absolute path in list type

def allFileList(rootPath):
  allFileList=[]
  for root, dirs, files in os.walk( top=rootPath, topdown=False):
    for name in files:
      
      pwd = os.path.join(root, name)
      #print(pwd)
      
      if os.path.getsize(pwd) > 1024 :
        allFileList.append(pwd)
         
  return allFileList

#allfiles = allFileList("/home/u/cityscsv")
allfiles = allFileList(startPathh) # use your Path

for f in allfiles:
  print(f)

  with open(f, newline='') as csvFile:
    # 1.直接讀取：讀取 CSV 檔案內容
    citys = {
    "A_lvr_land":"台北市","B_lvr_land":"台中市","C_lvr_land":"基隆市",
    "D_lvr_land":"台南市","E_lvr_land":"高雄市","F_lvr_land":"新北市",
    "G_lvr_land":"宜蘭縣","H_lvr_land":"桃園市","I_lvr_land":"嘉義市",
    "J_lvr_land":"新竹縣","K_lvr_land":"苗栗縣","M_lvr_land":"南投縣",
    "N_lvr_land":"彰化縣","O_lvr_land":"新竹市","P_lvr_land":"雲林縣",
    "Q_lvr_land":"嘉義縣","T_lvr_land":"屏東縣","U_lvr_land":"花蓮縣",
    "V_lvr_land":"台東市","W_lvr_land":"金門縣","X_lvr_land":"澎湖縣",
    "Z_lvr_land":"連江縣"
    }

    #we split the path and get the file name at the last word
    c = f.split("/")
    c = c[-1]  
    c = c[:10]
    #Read key word to get the value that is city name
    city = citys[c]
    #print(city)

    # 2.自訂分隔符號：讀取 CSV 檔案內容
    rows = csv.reader(csvFile, delimiter=',')
    
    #"covert to list type and delete previous two value ,so we start from [2:]"
    rows=list(rows)
    rows = rows[2:]

    tmpfile=[]
    tmp=[]
    #將剛剛取出的城市名藉由insert方法塞入tmp的開頭
    tmp.insert(0,city)
    for row in rows:
      list(row)

      #民國年轉西元年，因為長度不同ex: 民國77年107年，所以多寫一個if判斷式
      data = row[7]  #extract culumn 8 交易年月日
      months = data[3:5]
      days = data[5:7]
      if len(data) == 7:
        years=str(int(data[:3])+1911)
      elif len(data) == 6:
        years=str(int(data[:2])+1911)
      else:
        years=str("NULL")

      #整理完的值，直接append在list最後面不取代原值
      row.append(str(years)+"/"+months+"/"+days)

      if row[14]=="":
        row.append("NULL")
        #print(row[14])
      else:
        databuild = row[14]  #extract culumn 15 建築完成年月
        months = databuild[3:5]
        days = databuild[5:7]
        if len(databuild) == 7:
          years=str(int(databuild[:3])+1911)
        elif len(databuild) == 6:
          years=str(int(databuild[:2])+1911)
        else:
          years=str("NULL")

        row.append(str(years)+"/"+months+"/"+days)

      #tmp裡存的是城市名再加上處理完的raw
      row = tmp + row 
      #append回file
      tmpfile.append(row)
    #做完一個file顯示down1 file並將此file存入down1 list
    print("down 1 file")
    down1file= down1file+tmpfile
   

with open('output.csv', 'w', newline='') as csvfile:
  writer = csv.writer(csvfile)
  # 寫入二維表格
  writer.writerows(down1file)


