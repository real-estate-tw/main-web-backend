#!/usr/bin/python3
import json
from pymongo import MongoClient

# from operate_db.mongo import check_mongo_url
# from operate_db.mysql import check_host

from  mongo import check_mongo_url

def insert_data_mongodb():

	with open("./data/real_estate_data.json") as json_file:
		data = json.load(json_file)
		#print(data)

	#connectDB()
	url = check_mongo_url()
	#client = MongoClient('mongodb://localhost:27017/')
	
	client = MongoClient(url)
	db = client.web
	
	#print(operation.drop())
	ret = db.web.insert_one(data)
	print(ret.inserted_id )

if __name__ == '__main__':
	insert_data_mongodb()
